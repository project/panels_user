
Panels User is a central platform for organizing and managing all user-related
content. It's designed to play nicely with pretty much all existing user-related
content implementations.  
